<?php

namespace FritsStegmann\LaravelDashboard\Commands;

use App\User;
use FritsStegmann\LaravelDashboard\Auth\JWTGeneratorService;
use Illuminate\Console\Command;

class GenerateJWTCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jwt:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(JWTGeneratorService $service)
    {
        $id = $this->ask('What is the user id?');

        $user = User::where('id', $id)->first();

        if ($user) {
            print($service->generate($user->id));
        };

        return 0;
    }
}