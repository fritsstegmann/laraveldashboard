<?php

namespace FritsStegmann\LaravelDashboard\Commands;

use App\User;
use Illuminate\Console\Command;

class UserCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('What is the user name?');
        $email = $this->ask('What is the user email?');
        $password = \Hash::make($this->ask('What is the user password?'));

        User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);
    }
}
