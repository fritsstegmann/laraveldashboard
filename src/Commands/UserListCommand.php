<?php

namespace FritsStegmann\LaravelDashboard\Commands;

use App\User;
use Illuminate\Console\Command;

class UserListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        print('Name' . "\t");
        print('Email' . "\t");
        print("\n");

        foreach($users as $user) {
          print($user->name . "\t");
          print($user->email . "\t");
          print("\n");
        }
    }
}
