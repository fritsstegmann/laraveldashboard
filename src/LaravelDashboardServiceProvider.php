<?php

namespace FritsStegmann\LaravelDashboard;

use FritsStegmann\LaravelDashboard\Auth\JWTGeneratorService;
use FritsStegmann\LaravelDashboard\Auth\JwtGuard;
use FritsStegmann\LaravelDashboard\Commands\GenerateJWTCommand;
use FritsStegmann\LaravelDashboard\Commands\UserCreateCommand;
use FritsStegmann\LaravelDashboard\Commands\UserListCommand;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class LaravelDashboardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param Kernel $kernel
     */
    public function boot(Kernel $kernel)
    {
        $this->publishes([
            __DIR__ . '/config/jwt.php' => config_path('jwt.php'),
        ]);

        $this->publishes([
            __DIR__ . '/assets/dist' => public_path('vendor/dashboard'),
        ], 'public');

        $this->loadViewsFrom(__DIR__.'/views', 'dashboard');

        $this->publishes([
            __DIR__.'/views' => resource_path('views/vendor/dashboard'),
        ]);

        Auth::extend('jwt', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new JwtGuard(
                Auth::createUserProvider($config['provider']),
                $app['request']
            );
        });

        if ($this->app->runningInConsole()) {
            $this->commands([
                UserCreateCommand::class,
                UserListCommand::class,
                GenerateJWTCommand::class
            ]);
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(JWTGeneratorService::class, function () {
            return new JWTGeneratorService();
        });
    }
}