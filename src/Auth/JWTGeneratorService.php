<?php
/**
 * Created by PhpStorm.
 * User: frits
 * Date: 7/20/18
 * Time: 11:34 PM
 */

namespace FritsStegmann\LaravelDashboard\Auth;


use Firebase\JWT\JWT;

class JWTGeneratorService
{
    public function generate($identifier) {

        $password = config('app.key') . config('jwt.secret');
        $password = \hash('sha256', $password);

        $method = 'aes-256-cbc';
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

        $encrypted = base64_encode(openssl_encrypt($identifier, $method, $password, OPENSSL_RAW_DATA, $iv));


        return JWT::encode(['sub' => $encrypted], config('jwt.secret'));
    }
}